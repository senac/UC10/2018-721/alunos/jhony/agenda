//Bibliotecas inseridas quando oram chamadas alguma funções.
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class Agenda {

    public static void main(String[] args) {

        List<Contato> lista = new ArrayList<>(); //Cria a lista de contatos baseada na classe contatos

        for (int i = 0; i < 3; i++) {

            Contato pessoa = new Contato();  //Cria o objeto contato toda vez que passar pelo (for)
            pessoa.nome = JOptionPane.showInputDialog("Nome"); //Pedir o nome do contato
            pessoa.telefone = JOptionPane.showInputDialog("Telefone"); // Pedir o telefone do contato
        
        
            lista.add(pessoa);//Adiciona o contato preenchido na lista

            JOptionPane.showMessageDialog(null, "Total de contatos: " + lista.size()); //Imprimir o total de contatos
        }
        
            //Vai imprimir todos os contatos da lista.
            
            System.out.println("Imprimindo a lista de contatos...........");
            for (int i = 0; i < lista.size(); i++) {
                System.out.println(lista.get(i).nome + " - " + lista.get(i).telefone);
            }

        }

    }


