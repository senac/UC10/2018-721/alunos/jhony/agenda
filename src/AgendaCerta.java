
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class AgendaCerta {

    public static void main(String[] args) {

        List<Contato> lista = new ArrayList<>();
        int opcao;
        String aux;
        int count = 0;
        String j = "";

        do {

            String menu = "############### Agenda de contatos ###############\n"
                    + "                                          Contatos " + lista.size() + "\n"
                    + " 1 - Novo contato\n"
                    + " 2 - Buscar contato por nome\n"
                    + " 3 - Remover contato\n"
                    + " 4 - Listar todos os contatos\n"
                    + " 5 - Sobre a agenda\n"
                    + " 0 - Sair\n";

            aux = JOptionPane.showInputDialog(menu);
            opcao = Integer.parseInt(aux);

            switch (opcao) {
                case 1:
                    Contato pessoa = new Contato();

                    pessoa.nome = JOptionPane.showInputDialog("Digite o nome");
                    pessoa.telefone = JOptionPane.showInputDialog("Digite o telefone");
                    lista.add(pessoa);
                    JOptionPane.showMessageDialog(null, "Salvo com sucesso");
                    break;
                case 2:
                    aux = JOptionPane.showInputDialog("Digite um nome");
                    for (int i = 0; i < lista.size(); i++) {
                        if (lista.get(i).nome.equalsIgnoreCase(aux)) {
                            JOptionPane.showMessageDialog(null, lista.get(i).nome + " - " + lista.get(i).telefone);
                        } else {
                            JOptionPane.showMessageDialog(null, "Contato não encontrado");
                        }
                    }
                    break;
                case 3:
                    aux = JOptionPane.showInputDialog("Digite um nome");
                    for (int i = 0; i < lista.size(); i++) {
                        if (lista.get(i).nome.equalsIgnoreCase(aux)) {
                            lista.remove(i);
                            JOptionPane.showMessageDialog(null, "Contato removido com sucesso");
                        } else {
                            JOptionPane.showMessageDialog(null, "Contato nao existe");
                        }
                    }
                    break;
                case 4:
                    for (int i = 0; i < lista.size(); i++) {
                        j += lista.get(i).nome + " - " + lista.get(i).telefone + "\n";
                    }
                    JOptionPane.showMessageDialog(null, j);
                    break;
                case 5:
                    JOptionPane.showMessageDialog(null, "Deselvolvido por: Jhony Tavares\n" + "Turma: 721 de 2019", "Sobre", JOptionPane.INFORMATION_MESSAGE);
                    break;
                case 0:
                    System.exit(opcao);
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Opção invalida !!!!");
                    break;

            }
        } while (true);

    }
}
